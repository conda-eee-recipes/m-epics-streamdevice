m-epics-streamdevice conda recipe
=================================

Home: https://bitbucket.org/europeanspallationsource/m-epics-streamdevice

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS streamdevice module
