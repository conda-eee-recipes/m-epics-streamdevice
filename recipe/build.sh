#!/bin/bash

# Build the EPICS module
# Override PROJECT and LIBVERSION
# - PROJECT can't be guessed from the working directory
# - If we apply patches, the version will be set to the username
make PROJECT=streamdevice LIBVERSION=2.7.7
make PROJECT=streamdevice LIBVERSION=2.7.7 install

# Clean builddir between variants builds
rm -rf builddir
